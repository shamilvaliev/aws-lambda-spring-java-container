package com.tutorial.repository;

import com.tutorial.service.Company;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import software.amazon.awssdk.enhanced.dynamodb.DynamoDbEnhancedClient;
import software.amazon.awssdk.enhanced.dynamodb.DynamoDbTable;
import software.amazon.awssdk.enhanced.dynamodb.Key;
import software.amazon.awssdk.enhanced.dynamodb.TableSchema;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CompanyRepository {

    private DynamoDbEnhancedClient enhancedClient;

    @Value("${companyTable}")
    private String companyTableName;

    public CompanyRepository(DynamoDbEnhancedClient enhancedClient) {
        this.enhancedClient = enhancedClient;
    }

    public Company getOne(String companyId) {
        DynamoDbTable<Company> companyTable = enhancedClient.table(companyTableName, TableSchema.fromBean(Company.class));
        Key key = Key.builder().partitionValue(companyId).build();
        return companyTable.getItem(r -> r.key(key));
    }

    public List<Company> getList() {
        DynamoDbTable<Company> companyTable = enhancedClient.table(companyTableName, TableSchema.fromBean(Company.class));
        return companyTable.scan().items().stream().collect(Collectors.toList());
    }
}
