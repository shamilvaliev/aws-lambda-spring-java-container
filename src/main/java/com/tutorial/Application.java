package com.tutorial;

import com.tutorial.controller.CompanyController;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.web.servlet.HandlerAdapter;
import org.springframework.web.servlet.HandlerMapping;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;
import software.amazon.awssdk.enhanced.dynamodb.DynamoDbEnhancedClient;
import software.amazon.awssdk.http.SdkHttpClient;
import software.amazon.awssdk.http.urlconnection.UrlConnectionHttpClient;
import software.amazon.awssdk.services.dynamodb.DynamoDbClient;

@SpringBootApplication
@Import({CompanyController.class })
public class Application {

    @Value("${logging.level.root:OFF}")
    String message = "";

    @Bean
    public HandlerMapping handlerMapping() {
        return new RequestMappingHandlerMapping();
    }


    @Bean
    public HandlerAdapter handlerAdapter() {
        return new RequestMappingHandlerAdapter();
    }

    private SdkHttpClient httpClient() {
        return UrlConnectionHttpClient.builder().build();
    }

    @Bean
    public DynamoDbClient dynamoDbClient() {
        //return DynamoDbClient.create();
        return DynamoDbClient.builder()
                .httpClient(httpClient())
                /*.credentialsProvider(EnvironmentVariableCredentialsProvider.create())
                .region(Region.US_EAST_1)
                .endpointOverride(URI.create("https://dynamodb.us-east-1.amazonaws.com"))
                .overrideConfiguration(ClientOverrideConfiguration.builder().build())*/
                .build();
    }

    @Bean
    public DynamoDbEnhancedClient dynamoEnhancedClient() {
        return DynamoDbEnhancedClient.builder().dynamoDbClient(dynamoDbClient()).build();
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}