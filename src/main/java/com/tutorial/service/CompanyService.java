package com.tutorial.service;

import com.tutorial.repository.CompanyRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CompanyService {

    private CompanyRepository companyRepository;

    public CompanyService(CompanyRepository companyRepository) {
        this.companyRepository = companyRepository;
    }

    public Company getCompany(String companyId) {
        return companyRepository.getOne(companyId);
    }

    public List<Company> getCompanies() {
        return companyRepository.getList();
    }
}
